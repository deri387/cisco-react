import logo from "./logo.svg";
import "./App.css";
import React from "react";
// CUCM Jabber user
var username = "v1407user1";
var password = "c77939";
// CUCM hosts
var cucmServers = ["dcloud-sng-anyconnect.cisco.com"];

function App() {
  const [currentServer, setCurrentServer] = React.useState(0);
  const [lastAuthError, setLastAuthError] = React.useState("None");
  const [currentDevice, setCurrentDevice] = React.useState(null);
  const [currentConversation, setCurrentConversation] = React.useState(null);
  const [nativeConversationWindow, setNativeConversationWindow] =
    React.useState(null);
  const [isCwicInit, setIsCwicInit] = React.useState(false);
  const [isAuthorized, setIsAuthorized] = React.useState(false);
  const [isAuthenticated, setIsAuthenticated] = React.useState(false);
  const [isSigning, setIsSigning] = React.useState(false);
  const [isInProgress, setIsInProgress] = React.useState(false);
  const [targetCall, setTargetCall] = React.useState("");
  const [connectionStatus, setConnectionStatus] = React.useState("");
  const [directoryNumber, setDirectoryNumber] = React.useState("");
  const [enabledCall, setEnabledCall] = React.useState(true);
  const [enabledEndCall, setEnabledEndCall] = React.useState(true);
  const [statusCall, setStatusCall] = React.useState("");
  React.useEffect(() => {
    // Handlers for plugin init success/failure/error
    window.cwic.SystemController.addEventHandler(
      "onInitialized",
      onCwicInitialized
    );
    window.cwic.SystemController.addEventHandler(
      "onInitializationError",
      function (error) {
        alert("Initialization error: " + error.errorData.reason);
      }
    );
    window.cwic.SystemController.addEventHandler(
      "onAddonConnectionLost",
      () => {
        alert("Add-On connection lost!");
      }
    );
    // Handlers for user accepting/rejecting mic/camera permission
    window.cwic.SystemController.addEventHandler(
      "onUserAuthorized",
      onUserAuthorized
    );
    window.cwic.SystemController.addEventHandler(
      "onUserAuthorizationRejected",
      function () {
        alert("Audio/Video user authorization rejected!");
      }
    );
    // Init the plugin/library
    window.cwic.SystemController.initialize();
    return () => {
      window.cwic.SystemController.removeEventHandler("onInitialized");
      window.cwic.SystemController.removeEventHandler("onAddonConnectionLost");
      window.cwic.SystemController.removeEventHandler("onUserAuthorized");
      window.cwic.SystemController.removeEventHandler(
        "onUserAuthorizationRejected"
      );
    };
  }, []);

  const onCwicInitialized = () => {
    setIsCwicInit(true);
  };

  const onUserAuthorized = () => {
    setIsAuthorized(true);
    window.cwic.CertificateController.addEventHandler(
      "onInvalidCertificate",
      onInvalidCertificate
    );
    window.cwic.LoginController.addEventHandler(
      "onCredentialsRequired",
      onCredentialsRequired
    );
    window.cwic.LoginController.addEventHandler("onSignedOut", onSignedOut);
    window.cwic.LoginController.addEventHandler("onSigningIn", onSigningIn);
    window.cwic.LoginController.addEventHandler(
      "onAuthenticationStateChanged",
      onAuthenticationStateChanged
    );
    window.cwic.LoginController.addEventHandler(
      "onAuthenticationFailed",
      onAuthenticationFailed
    );
    window.cwic.TelephonyController.addEventHandler(
      "onTelephonyDeviceListChanged",
      onTelephonyDeviceListChanged
    );
    window.cwic.TelephonyController.addEventHandler(
      "onConnectionStateChanged",
      onConnectionStateChanged
    );
    window.cwic.TelephonyController.addEventHandler(
      "onConversationOutgoing",
      onConversationOutgoing
    );
    window.cwic.TelephonyController.addEventHandler(
      "onConversationStarted",
      onConversationStarted
    );
    window.cwic.TelephonyController.addEventHandler(
      "onConversationEnded",
      onConversationEnded
    );
    window.cwic.LoginController.setCUCMServers([cucmServers[currentServer]]);
    window.cwic.LoginController.setTFTPServers([cucmServers[currentServer]]);
    window.cwic.LoginController.signIn();
  };

  const onInvalidCertificate = (invalidCertificate) => {
    if (
      window.confirm(
        "Authentication error: CUCM SSL certificate invalid (please accept)"
      )
    ) {
      window.cwic.CertificateController.acceptInvalidCertificate(
        invalidCertificate
      );
    }
  };

  const onCredentialsRequired = (content) => {
    switch (lastAuthError) {
      case "None":
        setIsSigning(true);
        window.cwic.LoginController.setCUCMServers([
          cucmServers[currentServer],
        ]);
        window.cwic.LoginController.setTFTPServers([
          cucmServers[currentServer],
        ]);
        window.cwic.LoginController.setCredentials(username, password);
        break;
      case "Connection":
        window.cwic.LoginController.resetUserData();
        setLastAuthError("None");
        break;
      case "Credentials":
        break;
    }
  };

  const onSigningIn = () => {
    setIsSigning(true);
  };

  const onSignedOut = () => {
    setIsSigning(false);
    setIsInProgress(false);
    setIsAuthorized(false);
  };

  const onAuthenticationFailed = (error) => {
    switch (error) {
      case "CouldNotConnect":
      case "ClientCertificateError":
      case "SSLConnectError":
        alert(
          `Authentication error!  Could not connect to: ${cucmServers[currentServer]}`
        );
        let server =
          currentServer == cucmServers.length - 1 ? 0 : ++currentServer;
        setCurrentServer(server);
        setLastAuthError("Connection");
        break;
      case "InvalidCredentials":
      case "InvalidToken":
        alert("Authentication error!  Invalid credentials");
        setLastAuthError("Credentials");
        break;
      case "NoCredentialsConfigured":
        setLastAuthError("None");
        break;
      default:
        alert(`Authentication error! Message: ${error}`);
        break;
    }
  };

  const onAuthenticationStateChanged = (state) => {
    switch (state) {
      case "InProgress":
        setIsInProgress(true);
        setIsAuthenticated(false);
        break;
      case "Authenticated":
        setIsInProgress(true);
        setIsAuthenticated(true);

        break;
      case "NotAuthenticated":
        setIsInProgress(false);
        setIsAuthenticated(false);
    }
  };

  const onTelephonyDeviceListChanged = () => {
    if (currentDevice) {
      for (const device of window.cwic.TelephonyController.telephonyDevices) {
        if (device.name == currentDevice) {
          setDirectoryNumber(device.activeLine);
        }
      }
      return;
    }

    for (const device of window.cwic.TelephonyController.telephonyDevices) {
      if (device.type == "Cisco Unified Client Services Framework") {
        setCurrentDevice(device.name);
        device.connect(true);
        break;
      }
    }
  };

  const onConnectionStateChanged = (state) => {
    setConnectionStatus(state);
    switch (state) {
      case "Disconnected": {
        setEnabledCall(true);
        setEnabledEndCall(false);
        setStatusCall("");
        break;
      }
      case "Connecting": {
        setEnabledCall(true);
        break;
      }
      case "Connected": {
        setEnabledCall(false);
        setNativeConversationWindow(
          window.cwic.WindowController.getNativeConversationWindow()
        );
        setStatusCall("Onhook");
        break;
      }
    }
  };

  const makeCallClick = () => {
    setEnabledCall(true);
    window.cwic.TelephonyController.startVideoConversation(targetCall);
  };

  const endCallClick = () => {
    if (currentConversation && currentConversation.capabilities.canEnd) {
      currentConversation.end();
    }
  };

  const onConversationOutgoing = (conversation) => {
    setCurrentConversation(conversation);
    setEnabledEndCall(false);
    setEnabledCall(true);
    setStatusCall("Ringing");
  };

  const onConversationStarted = (conversation) => {
    // Dock the video window so it appears stationary in the page
    nativeConversationWindow.dock(
      document.getElementById("remote-video-container")
    );
    nativeConversationWindow.showVideoForConversation(conversation);

    setEnabledEndCall(false);
    setStatusCall("Talking");
  };

  const onConversationEnded = (conversation) => {
    nativeConversationWindow.hide();

    setEnabledEndCall(true);
    setEnabledCall(false);
    setStatusCall("Onhook");
  };
  return (
    <React.Fragment>
      <div>
        <input type="checkbox" checked={isCwicInit} onClick={() => false} />
        CWIC extension initialized
      </div>
      <div>
        <input type="checkbox" checked={isAuthorized} onClick={() => false} />
        Audio/Video authorized
      </div>
      <div>
        <input type="checkbox" checked={isSigning} onClick={() => false} />
        Signing in&nbsp;{isSigning ? cucmServers[currentServer] : ""}
      </div>
      <div>
        <input type="checkbox" checked={isInProgress} onClick={() => false} />
        Authentication in progress...
      </div>
      <div>
        <input
          type="checkbox"
          checked={isAuthenticated}
          onClick={() => false}
        />
        Authenticated
      </div>
      <br />
      <div>
        Selected device: <span>{currentDevice}</span>{" "}
        <span
          style={{
            color:
              connectionStatus === "Disconnected"
                ? "red"
                : connectionStatus === "Connecting"
                ? "goldenrod"
                : connectionStatus === "Connected"
                ? "green"
                : "auto",
          }}
        >
          {connectionStatus}
        </span>
      </div>
      <br />
      <div>
        Directory Number: <span>{directoryNumber}</span>
      </div>
      <br />
      <label for="numtodial">Number to dial:</label>
      <input
        type="text"
        id="destination"
        onChange={(e) => setTargetCall(e.target.value)}
      />

      <button type="button" disabled={enabledCall} onClick={makeCallClick}>
        Make Call
      </button>
      <button type="button" disabled={enabledEndCall} onClick={endCallClick}>
        End Call
      </button>
      <span>{statusCall}</span>
      <br />
      <br />
      <div id="remote-video-container" class="rvc"></div>
    </React.Fragment>
  );
}

export default App;
